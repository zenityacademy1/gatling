object Config {
  val app_url_login = "https://petstore.swagger.io/v2/user/login"
  val app_url_createUser = "https://petstore.swagger.io/v2/user"

  val users = Integer.getInteger("users", 2).toInt
  val rampUp = Integer.getInteger("rampup", 1).toInt
}