import Config.app_url_createUser
import io.gatling.http.Predef._
import io.gatling.core.Predef._

object CreateUserRequest {

  //val sentHeaders = Map("Authorization" -> "bearer ${token}")

  val create_user = exec(http("Create User Request")
    .post(app_url_createUser )
    .headers(Map("accept" -> "application/xml"))
    .headers(Map("Content-Type" -> "application/json"))
    .body(StringBody("""{
              "id": 0,
              "username": "toto666",
              "firstName": "toto666",
              "lastName": "toto666",
              "email": "toto666@toto.fr",
              "password": "toto666",
              "phone": "0666666666",
              "userStatus": 1
            }""")).asJson
    .check(bodyString.saveAs("responceCreateUser"))
    .check(status is 200))
    .exec { session => println(session("responceCreateUser").as[String]); session}
}