import io.gatling.core.Predef.scenario

object LoginCreateScenario {

    val loginCreateUserScenario = scenario("Create or Login User Scenario")
      .exec(UserLoginRequest.login_user)
      .exec(CreateUserRequest.create_user)

}
