import Config.app_url_login
import io.gatling.http.Predef._
import io.gatling.core.Predef._


object UserLoginRequest {

  //val sentHeaders = Map("Authorization" -> "bearer ${token}")

  val login_user = exec(http("Login User Request")
    .get(app_url_login)
    .check(bodyString.saveAs("responceLoginUser"))
    .check(status is 200))
    .exec { session => println(session("responceLoginUser").as[String]); session}

}